=encoding utf8

=head1 NAME

B<git-bump> - create Git release commits and tags with changelogs

=head1 SYNOPSIS

B<git bump> [B<major> | B<minor> | B<point> | I<VERSION>]

B<git bump log>

B<git bump next>

B<git bump redo>

B<git bump show> [I<VERSION>]

B<git bump help> [I<COMMAND>]

=head1 DESCRIPTION

B<git bump> automates the best practices for doing releases for
code bases stored in Git:

=over

=item * Update version-related minutiae in the code base.

=item * Commit with a message like projectname 1.2.3.

=item * Create a signed, annotated tag with the same message with a name like v1.2.3.

=item * Include release notes in the release commit.

=back

=head1 COMMANDS

B<git bump> [B<--force>|B<-f>] [B<major> | B<minor> | B<point> | I<VERSION>]

=over

Bump the version according to the argument:

=over

=item * B<major>: bump the first number in the previous version, and reset everything afterwards to zero

=item * B<minor>: bump the second number in the previous version, and reset everything afterwards to zero

=item * B<point>: bump the third number in the previous version, and reset everything afterwards to zero

=item * I<VERSION>: bump to the exact version I<VERSION>

=item * no argument: bump the rightmost number in the previous version. For example, 1.2.3-rc4 becomes 1.2.3-rc5, while 6.7 becomes 6.8

=back

Use B<--force> to re-tag the release.

If there are no changes staged, changes from the previous release commit will be replayed, replacing the appropriate version numbers.

The commit message body will be pre-populated with a bulleted list of commit messages since the previous release.

See B<L</USAGE>> for more details.

=back

B<git bump log> [I<...>]

=over

Show the Git log since the last release. Any arguments are passed to B<git log>.

=back

B<git bump next>

=over

Show the version number that would be released without performing any actual changes.

=back

B<git bump redo>

=over

Amend the previous release and re-tag. Can only be used directly at the previous release commit.

=back

B<git bump show> [B<--version-only>] [I<VERSION>]

=over

Show the most recent or given release. By default, prints the commit message.
Use B<--version-only> to print the version number instead.

=back


B<git bump help> [I<COMMAND>]

=over

Print a short usage notice.

=back

=head1 USAGE

=head2 Initial release

Stage the changes needed to create the release (this could be the entire
repository if it's an initial commit), and run B<git bump> I<< <version> >>,
where I<< <version> >> is the version you want to release (try 1.0.0).
You'll be greeted with a familiar sight:

    spline-reticulator 1.0.0
    
    # Please enter the commit message for your changes. Lines starting
    # with '#' will be ignored, and an empty message aborts the commit.

Adjust the project name if necessary, and save and quit the editor.
Your commit and tag will be created, and you'll be shown instructions for
pushing once you're sure everything is okay.

=head2 Second release

This is where the fun begins. Stage the changes necessary for release,
and run B<git bump>.

The commit message body will be pre-populated with a bulleted list of
commit messages since the previous release. A good practice is to heavily
edit this into a higher level list of changes by discarding worthless
messages like typo fixes and making related commits into a single bullet
point. If you aren't interested in this practice, delete the body and
B<git bump> won't bother you with it again.

=head2 Subsequent releases

On subsequent releases, if no changes are staged, B<git bump> will
replay the previous release commit, replacing the appropriate version
numbers. This works fine as long as your version numbers are committed
as literal strings. If you're doing something more clever like C<MAJOR = 1>
and C<MINOR = 2>, you'll have to do the edit by hand and stage it.

=head2 Existing projects

You'll need to create one existing release commit and tag in the proper
format by hand, if your project doesn't already have one. After that
you can use B<git bump> normally.

=head1 COPYRIGHT

Copyright (C) 2014, 2015 Tim Pope L<< <code@tpope.net> >>

License: MIT/Expat

This manual page is based on the README document by Tim Pope.
It was reformatted and adapted by Andrej Shadura L<< <andrewsh@debian.org> >>
for the Debian project, and is distributed under the same license as the
original project.
